#!/bin/sh

echo "hello from wpe image"

udevd &
udevadm trigger

# link displayserver sockets to shared volume
ln -s /tmp/displayserver/.X11-unix/ /tmp/
ln -s /tmp/displayserver/.wayland/ /tmp/

export XDG_RUNTIME_DIR=/tmp/.wayland
export WAYLAND_DISPLAY=wayland-0

cog --platform=fdo ${WPE_URL:-https://webglsamples.org/aquarium/aquarium.html}

