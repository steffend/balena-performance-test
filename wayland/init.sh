#!/bin/bash

export DBUS_SYSTEM_BUS_ADDRESS=unix:path=/host/run/dbus/system_bus_socket

# make sure sway is run as root
chmod a+s /usr/bin/sway
# first switch back to vt1
chvt 1
# run display init on vt7
openvt -c 7 -f -- ./display-init.sh $(tty)

echo $(tty) > /tmp/.balena_log_tty

sleep infinity
